﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinChecker : MonoBehaviour {

	public static void CheckForWin()
    {
        Node[] nodes = Node.Grid;

        bool win = true;

        foreach(Node node in nodes)
        {
            if (node.Pickup != null && node.Pickup is Pellet)
                win = false;
        }

        if(win)
        {
            Node.AllNodes = new List<Node>();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
