﻿using MachineLearning;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Eater : MonoBehaviour
{
    public static StateAgent Brain;

    private float moveProgress = 0;

    private Vector3 from, to;

    private bool canDoMove = true;

    private Node nodeToKill = null;

    [SerializeField] private float speed = 1;

    [SerializeField] private Node currentNode = null;

    private StateAction lastAction;

    public int Column
    {
        get
        {
            return Mathf.RoundToInt(transform.position.x);
        }

        set
        {
            transform.position = new Vector3(value, transform.position.y, transform.position.z);
        }
    }

    public int Row
    {
        get
        {
            return Mathf.RoundToInt(transform.position.z);
        }

        set
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, value);
        }
    }

    private State GetBoardState()
    {
        Node[] nodes = Node.Grid;

        string stateStr = Column + "," + Row;

        for(int i = 0; i < nodes.Length; i++)
        {
            stateStr += nodes[i].NodeString;
        }

        return new State(stateStr, currentNode.PossibleActions);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space) ||  (currentNode != null && currentNode.PossibleActions.Length == 0))
        {
            Node.AllNodes = new List<Node>();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            return;
        }

        if (currentNode == null)
            currentNode = Node.GetAtPosition(Column, Row);

        if (canDoMove)
        {
            canDoMove = false;
            if (Brain == null)
            {
                Brain = new StateAgent(GetBoardState(), 15);
            }

            Debug.Log("States Recorded: " + Brain.LearnedStates + ", evaluated: " + Brain.EvaluatedActions);

            if (currentNode.Pickup != null)
            {
                if (currentNode.Pickup is Pellet)
                {
                    Brain.EvaluateAbsolute(100000);
                }
                else
                {
                    Brain.EvaluateAbsolute(-50);
                }

                Destroy(currentNode.Pickup.gameObject);
            }
            else
            {
                Brain.EvaluateAbsolute(100);
            }

            if(lastAction != null)
            {
                Brain.PerformStateAction(lastAction, GetBoardState());
            }

            WinChecker.CheckForWin();
            
            lastAction = Brain.GetChosenActionForCurrentState();
            
            from = currentNode.transform.position;

            nodeToKill = currentNode;

            switch (lastAction.ActionString)
            {
                case "left":
                    currentNode = Node.GetAtPosition(currentNode.Column - 1, currentNode.Row);
                    break;
                case "right":
                    currentNode = Node.GetAtPosition(currentNode.Column + 1, currentNode.Row);
                    break;
                case "up":
                    currentNode = Node.GetAtPosition(currentNode.Column, currentNode.Row - 1);
                    break;
                case "down":
                    currentNode = Node.GetAtPosition(currentNode.Column, currentNode.Row + 1);
                    break;
            }

            to = currentNode.transform.position;
            moveProgress = 0;
        }

        if (moveProgress >= 1)
        {
            if (nodeToKill != currentNode)
            {
                Node.AllNodes.Remove(nodeToKill);
                Destroy(nodeToKill.gameObject);
            }

            canDoMove = true;
        }
        else
        {
            moveProgress += Time.deltaTime * speed;

            transform.position = Vector3.Lerp(from, to, moveProgress);
        }
	}
}
