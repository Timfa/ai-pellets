﻿using MachineLearning;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Node : MonoBehaviour
{
    public static List<Node> AllNodes = new List<Node>();
    [SerializeField] private Pickup prefabToSpawn;
    public Pickup Pickup;
    
    public static Node[] Grid
    {
        get
        {
            return AllNodes.OrderBy(o => o.Column).ThenBy(o => o.Row).ToArray();
        }
    }

    public int Column
    {
        get
        {
            return Mathf.RoundToInt(transform.position.x);
        }

        set
        {
            transform.position = new Vector3(value, transform.position.y, transform.position.z);
        }
    }

    internal void UnregisterPickup(Pickup pickup)
    {
        if (pickup == Pickup)
            Pickup = null;
    }

    public int Row
    {
        get
        {
            return Mathf.RoundToInt(transform.position.z);
        }

        set
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, value);
        }
    }

    public StateAction[] PossibleActions
    {
        get
        {
            List<StateAction> actions = new List<StateAction>();

            if (GetAtPosition(Column - 1, Row) != null)
                actions.Add(new StateAction("left", 300));

            if (GetAtPosition(Column + 1, Row) != null)
                actions.Add(new StateAction("right", 300));

            if (GetAtPosition(Column, Row - 1) != null)
                actions.Add(new StateAction("up", 300));

            if (GetAtPosition(Column, Row + 1) != null)
                actions.Add(new StateAction("down", 300));

            return actions.ToArray();
        }
    }

    public static Node GetAtPosition(Vector3 position)
    {
        int c = Mathf.RoundToInt(position.x);
        int r = Mathf.RoundToInt(position.z);

        return GetAtPosition(c, r);
    }

    public static Node GetAtPosition(int col, int row)
    { 
        foreach(Node node in Node.AllNodes)
        {
            if (node.Column == col && node.Row == row)
                return node;
        }

        return null;
    }

    public string NodeString
    {
        get
        {
            return (Pickup == null ? "Empty-" : Pickup.GetTypeString()) + Column + "," + Row;
        }
    }

    private void Awake()
    {
        Column = Column;
        Row = Row;

        if (prefabToSpawn != null)
        {
            Pickup = Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
            Pickup.Node = this;
        }

        AllNodes.Add(this);
    }
}
